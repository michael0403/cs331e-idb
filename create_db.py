import json
from models import db, Artist, Artist_Genre, Album, Track
from scripts.load_yaml import load_yaml

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(f'./scraped_data/{filename}', encoding='utf-8') as file:
        jsn = json.load(file)
        file.close()
    
    return jsn

def create_artists(dev: bool=False):
    """
    populate artist table
    """
    if (dev):
        artists = load_json('developer_data/artists.json')
    else:
        artists = load_json('artists.json')
    for artist in artists:
        row = Artist(**artist)
        db.session.add(row)
        db.session.commit()
        del row
    del artists

def create_albums(dev: bool=False):
    """
    populate album table
    """
    if (dev):
        albums = load_json('developer_data/albums.json')
    else:
        albums = load_json('albums.json')
    for album in albums:
        # edge case where only a year is present
        if len(album["release_date"]) == 4:
            album["release_date"] += "-01-01"

        row = Album(**album)
        db.session.add(row)
        db.session.commit()
        del row
    del albums

def create_tracks(dev: bool=False):
    """
    populate track table
    """
    if (dev):
        tracks = load_json('developer_data/tracks.json')
    else:
        tracks = load_json('tracks.json')
    for track in tracks:
        row = Track(**track)
        db.session.add(row)
        db.session.commit()
        del row
    del tracks

def create_artist_genres(dev: bool=False):
    """
    populate artist genre table
    """
    if (dev):
        genres = load_json('developer_data/artist_genres.json')
    else:
        genres = load_json('artist_genres.json')
    for genre in genres:
        row = Artist_Genre(**genre)
        db.session.add(row)
        db.session.commit()
        del row
    del genres

def create_drops(dev: bool=False):
    """
    populate drops table
    """
    if (dev):
        drops = load_json('developer_data/drops.json')
    else:
        drops = load_json('drops.json')
    for drop in drops:
        track = Track.query.filter_by(id=drop['track_id']).first()
        artist = Artist.query.filter_by(id=drop['artist_id']).first()
        track.dropped_by.append(artist)
        db.session.commit()
        del track
        del artist
    del drops

def create_releases(dev: bool=False):
    """
    populate releases table
    """
    if (dev):
        releases = load_json('developer_data/releases.json')
    else:
        releases = load_json('releases.json')
    for release in releases:
        album = Album.query.filter_by(id=release['album_id']).first()
        artist = Artist.query.filter_by(id=release['artist_id']).first()
        if artist == None:
            print(f'none artist: {release["artist_id"]}')
            exit()
        album.released_by.append(artist)
        db.session.commit()
        del album
        del artist
    del releases

def setup_db(dev: bool=False):
    """Setup the database model and load data.

    Args:
        dev (bool, optional): Setup the database with a smaller set of data
            in order to load faster. Defaults to False.
    """
    try:
        db.drop_all()
        db.create_all()
    except Exception as e:
        print(e)
        print('************************')
        print('Creating database failed')
        print('************************')
    try:
        create_artists(dev)
        print(0, 'artists')
        create_artist_genres(dev)
        print(0, 'genres')
        create_albums(dev)
        print(0, 'albums')
        create_tracks(dev)
        print(0, 'tracks')
        create_releases(dev)
        print(0, 'releases')
        create_drops(dev)
        print(0, 'drops')
        print('all tables ingested')
    except Exception as e:
        print(e)
        print('*********************')
        print('Inserting data failed')
        print('*********************')

if __name__ == "__main__":
    setup_db(load_yaml('config.yaml')['developer_mode'])
