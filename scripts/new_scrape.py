"""
File: new_scrape.py
Authors: 
        Rory James

Scrapes data from the spotify API beginning with a few provided artists.
It then scrapes related tracks, albums, and artists up to a certain level
of iterations.

Requires num_iterations, and starting_artists to be specified.
"""

import json
import random
import time
import requests
from load_yaml import load_yaml
from oauth_token import request_token

access_token = request_token()
header = {"Authorization": f"Bearer {access_token}"}


# Get the starting artists and how many iterations
config = load_yaml("config.yaml")['scrape']
STARTING_ARTISTS = config['starting_artists']
NUM_ITERATIONS = config['num_iterations']

tracks_set = set()
albums_set = set()
artists_set = set()

tracks_batch = []
albums_batch = []
artists_batch = STARTING_ARTISTS

albums = []
artists = []
tracks = []
releases = []
drops = []
artist_genres = []

def request(url):
    response = requests.get(url, headers=header)
    if response.status_code == 429:
        print("Too many requests. Sleeping for 30 seconds.")
        time.sleep(30)
        return request(url)
    elif response.status_code == 200:
        return response.json()
    else:
        print(f"Error {response.status_code} requesting {url}")
        print(response.text)
        exit()

# Check that we are in the right directory when running
try:
    with open("scraped_data/developer_data/tracks.json", 'r') as f:
        pass
except FileNotFoundError:
    print("Check your current directory! This script must be run from the",
          "base directory and not the scripts folder!")
    raise Exception

# Start scraping
for i in range(1, NUM_ITERATIONS + 1):
    print("New batch of artists")

    # process artists
    for id in artists_batch:
        if id in artists_set:
            continue

        data = request(f"https://api.spotify.com/v1/artists/{id}")
        artists_set.add(id)
        artist = {"id": id}
        artist["name"] = data["name"]
        print(f"(Iteration {i}) Artist #{len(artists) + 1}: {artist['name']}")
        if len(data["images"]) > 0:
            artist["img_url"] = data["images"][0]["url"]
        else:
            artist["img_url"] = None
        artist["popularity"] = data["popularity"]
        artists.append(artist)

        for genre in data['genres']:
            artist_genres.append({'id': id, 'genre': genre})

        data = request(f"https://api.spotify.com/v1/artists/{id}/albums?limit=3&offset=0")
        max_albums = 1 if i >= 3 else 2 if i == 2 else 3
        for j in range(random.randint(0, min(max_albums, len(data["items"])))):
            albums_batch.append(data["items"][j]["id"])

    artists_batch = []

    # process albums
    for id in albums_batch:
        if id in albums_set:
            continue

        data = request(f"https://api.spotify.com/v1/albums/{id}")
        albums_set.add(id)
        album = {"id": id}
        album["name"] = data["name"]
        album["popularity"] = data["popularity"]
        album["label"] = data["label"]
        print(f"(Iteration {i}) Album #{len(albums) + 1}: {album['name']}")
        if len(data["images"]) > 0:
            album["img_url"] = data["images"][0]["url"]
        else:
            album["img_url"] = None
        album["release_date"] = data["release_date"]
        albums.append(album)

        for item in data["artists"]:
            if not item["id"] in artists_set:
                if i < NUM_ITERATIONS:
                    artists_batch.append(item["id"])
                else:
                    continue
            release = {"album_id": id, "artist_id": item["id"]}
            releases.append(release)
        
        data = request(f"https://api.spotify.com/v1/albums/{id}/tracks?limit=50")
        max_tracks = 1 if i >= 3 else 10 if i == 2 else 20
        for j in range(min(max_tracks, len(data["items"]))):
            tracks_batch.append(data["items"][j]["id"])
    
    albums_batch = []

    # process tracks
    for id in tracks_batch:
        if id in tracks_set:
            continue

        data = request(f"https://api.spotify.com/v1/tracks/{id}")
        tracks_set.add(id)
        track = {"id": id}
        track["name"] = data["name"]
        print(f"(Iteration {i}) Track #{len(tracks) + 1}: {track['name']}")
        track["popularity"] = data["popularity"]
        track["preview_url"] = data["preview_url"]
        track["duration_ms"] = data["duration_ms"]
        track["album_id"] = data["album"]["id"]
        tracks.append(track)

        for item in data["artists"]:
            if not item["id"] in artists_set:
                if i < NUM_ITERATIONS:
                    artists_batch.append(item["id"])
                else:
                    continue
            drop = {"track_id": id, "artist_id": item["id"]}
            drops.append(drop)
    
    tracks_batch = []

# Save the data
try:
    with open('scraped_data/tracks.json', 'w') as f:
        json.dump(tracks, f)

    with open('scraped_data/artists.json', 'w') as f:
        json.dump(artists, f)

    with open('scraped_data/albums.json', 'w') as f:
        json.dump(albums, f)

    with open('scraped_data/drops.json', 'w') as f:
        json.dump(drops, f)

    with open('scraped_data/releases.json', 'w') as f:
        json.dump(releases, f)
    
    with open('scraped_data/artist_genres.json', 'w') as f:
        json.dump(artist_genres, f)

    print(f"{len(tracks)} tracks, {len(albums)} albums, {len(artists)} artists")
    print("scraping completed!")
except FileNotFoundError:
    print("Check your current directory! This script must be run from the",
          "base directory and not the scripts folder!")
