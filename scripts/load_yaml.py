import yaml

def load_yaml(file: str) -> dict:
    """Reads the yaml file and returns it

    Args:
        file (str): YAML file to extract

    Returns:
        dict: Python equivalent dictionary of YAML file
    """
    with open(file) as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)