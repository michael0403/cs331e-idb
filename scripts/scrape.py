# I wrote this script to get additional track instances we could display for the project1 website
# It outputs a JSON file because that format is the easiest to paste as a python dictionary
# for project2, the script will likely need to output csv files which can be bulk-loaded into PostgreSQL
# the script in its current state will serve as a starting point for the project2 script

import requests
import base64
import json
from oauth_token import request_token

# CLIENT_ID = 'd0486ca98e7449dc922fa111e021c4f8'
# CLIENT_SECRET = '1bfc48a525294e7fac1f508f5ce41c49'

access_token = request_token()
header = {"Authorization": f"Bearer {access_token}"}

# def encode(string: str) -> str:
#   string_bytes = string.encode('ascii')
#   base64_bytes = base64.b64encode(string_bytes)
#   return base64_bytes.decode('ascii')

# def request_token() -> str:
#   headers = {
#     "Content-Type": "application/x-www-form-urlencoded",
#     "Authorization": "Basic " + encode(f"{CLIENT_ID}:{CLIENT_SECRET}")
#   }
#   data={"grant_type": "client_credentials"}
#   response = requests.post('https://accounts.spotify.com/api/token', headers=headers, data=data)
#   if response.status_code != 200:
#     print(f'Error: got status {response.status_code} when requesting an access token')
#     exit()
#   return response.json()["access_token"]

# access_token = request_token()

ALBUM_IDS = [
  "4894htPwC6zoiuTqUQwn4I",
  "4X6b6POxbjX9inC7TWQd54",
  "7itCnIV6547QPjB5pnRY6y"
]

tracks = []
track_ids = set()

artists = []
artist_ids = set()

albums = []

header = {"Authorization": f"Bearer {access_token}"}

for id in ALBUM_IDS:
    response = requests.get(f"https://api.spotify.com/v1/albums/{id}", headers=header)
    if response.status_code != 200:
        print(f'Error: got status {response.status_code} when scraping track ids')
        exit()
    data = response.json()

    album = {"album_id": id}
    album["album_name"] = data["name"]
    album["album_image"] = data["images"][0]["url"]
    album["album_release_date"] = data["release_date"]

    album["artists"] = []
    for item in data["artists"]:
       artist = {"artist_id": item["id"], "artist_name": item["name"]}
       album["artists"].append(artist)
       artist_ids.add(item["id"])

    album["list_of_tracks"] = []
    for item in data["tracks"]["items"]:
        track = {"track_id": item["id"], "name_track": item["name"]}
        album["list_of_tracks"].append(track)
        track_ids.add(item["id"])
    
    albums.append(album)

for id in track_ids:
    response = requests.get(f"https://api.spotify.com/v1/tracks/{id}", headers=header)
    if response.status_code != 200:
        print(f'Error: got status {response.status_code} when scraping track data')
        exit()
    track = {"id": id}
    data = response.json()
    track["name"] = data["name"]
    track["popularity"] = data["popularity"]
    track["preview_url"] = data["preview_url"]
    track["duration_ms"] = data["duration_ms"]
    track["artists"] = []
    for a in data["artists"]:
       artist = {"artist_id": a["id"], "artist_name": a["name"]}
       track["artists"].append(artist)
       artist_ids.add(a["id"])
    track["album_id"] = data["album"]["id"]
    track["album_name"] = data["album"]["name"]
    track["album_img_url"] = data["album"]["images"][0]["url"]
    tracks.append(track)

for id in artist_ids:
    response = requests.get(f"https://api.spotify.com/v1/artists/{id}", headers=header)
    if response.status_code != 200:
        print(f'Error: got status {response.status_code} when scraping artist data')
        exit()
    data = response.json()
    
    artist = {"artist_id": id}
    artist["artist_name"] = data["name"]
    artist["artist_img_url"] = data["images"][0]["url"]
    artist["popularity"] = data["popularity"]

    done = False
    for album in albums:
       if done: break
       for a in album["artists"]:
          if a["artist_id"] == id:
             artist["album_id"] = album["album_id"]
             artist["album_name"] = album["album_name"]
             artist["album_img_url"] = album["album_image"]
             done = True
             break
    
    done = False
    for track in tracks:
        if done: break
        for a in track["artists"]:
            if a["artist_id"] == id:
                artist["top_tracks_id"] = track["id"]
                artist["top_tracks"] = track["name"]
                done = True
                break
        
    artists.append(artist)

with open('scraped_data/tracks.json', 'w') as f:
    json.dump(tracks, f)

with open('scraped_data/artists.json', 'w') as f:
    json.dump(artists, f)

with open('scraped_data/albums.json', 'w') as f:
    json.dump(albums, f)

print("scraping completed!")

