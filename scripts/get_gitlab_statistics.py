"""Sends an API request to GitLab for project details.

Uses the `python-gitlab` package.

Typical usage example:

  data = get_gitlab_statistics(project_id, access_token)
"""
import gitlab
from typing import Tuple

def get_gitlab_statistics(project_id: int, access_token: str) -> Tuple[dict, dict]:
    """Get project details from the GitLab API

    Args:
        project_id (int): GitLab project ID. Defaults to None.
        access_token (str): Access token to get access to project API. Defaults to None.
    
    Returns:
        tuple: Contains a dictionary with member statistics and total statistics.
        For example:

        (
            {'member1': {'num_commits': 0, 'num_issues':0}},
            {'total_commits': 0, 'total_issues':0}
        )
    """    

    # Create gitlab object
    gl = gitlab.Gitlab(private_token=access_token)
    gl.auth()

    # Get access to the project
    project = gl.projects.get(project_id)
    # Only access commits on the main branch
    commits = project.commits.list(ref_name='main', all=True)
    issues = project.issues.list(get_all=True)

    # Count number of commits and issues for each person
    contributors = {}
    for commit in commits:
        author = commit.attributes['author_name']
        if author not in contributors:
            contributors[author] = {'num_commits': 0, 'num_issues': 0}
        contributors[author]['num_commits'] += 1

    for issue in issues:
        author = issue.attributes['author']['name']
        if author not in contributors:
            contributors[author] = {'num_commits': 0, 'num_issues': 0}
        contributors[author]['num_issues'] += 1

    # catch users with multiple commit signatures
    if ('Rory James' in contributors.keys()) and ('rjames187' in contributors.keys()):
        contributors['Rory James']['num_commits'] += contributors['rjames187']['num_commits']
        contributors['Rory James']['num_issues'] += contributors['rjames187']['num_issues']
        contributors.pop('rjames187')
    if ('Jazmin Reyna' in contributors.keys()) and ('lystalin' in contributors.keys()):
        contributors['Jazmin Reyna']['num_commits'] += contributors['lystalin']['num_commits']
        contributors['Jazmin Reyna']['num_issues'] += contributors['lystalin']['num_issues']
        contributors.pop('lystalin')
    if ('Saurelle N. J' in contributors.keys()) and ('myproject-SWE' in contributors.keys()):
        contributors['Saurelle N. J']['num_commits'] += contributors['myproject-SWE']['num_commits']
        contributors['Saurelle N. J']['num_issues'] += contributors['myproject-SWE']['num_issues']
        contributors.pop('myproject-SWE')

    totals = {
        'total_commits': len(commits),
        'total_issues': len(issues)
    }
    
    return (contributors, totals)