"""
File: oauth_token.py
Authors: 
        Rory James

Retrieves an access token for the spotify API.

Requires a client ID and client secret to obtain the access token.
"""

import base64
import requests
from load_yaml import load_yaml


# Get the tokens for spotify
config = load_yaml("tokens.yaml")['spotify']
CLIENT_ID = config['id']
CLIENT_SECRET = config['secret']

def encode(string: str) -> str:
    string_bytes = string.encode('ascii')
    base64_bytes = base64.b64encode(string_bytes)
    return base64_bytes.decode('ascii')

def request_token() -> str:
    headers = {
        "Content-Type": "application/x-www-form-urlencoded",
        "Authorization": "Basic " + encode(f"{CLIENT_ID}:{CLIENT_SECRET}")
    }
    data={"grant_type": "client_credentials"}
    response = requests.post('https://accounts.spotify.com/api/token', headers=headers, data=data)
    if response.status_code != 200:
        print(f'Error: got status {response.status_code} when requesting an access token')
        exit()
    return response.json()["access_token"]