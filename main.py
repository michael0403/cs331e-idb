#!/usr/bin/env python3

"""
File: main.py
Authors:
    Jazmin Reyna
    Rory James
    Michael Cheng
    Saurelle Ngaintse Jiotsa
    Steven Ortega

Runs a website
"""

import time

from flask import jsonify, render_template, request, Response
from scripts.get_gitlab_statistics import get_gitlab_statistics as get_gl_stats
from scripts.load_yaml import load_yaml
from models import app
from db.artist_api import getArtists, getOneArtist, countArtists, countMatchingArtists
from db.album_api import getAlbums, getOneAlbum, countAlbums, countMatchingAlbums
from db.track_api import getTracks, getOneTrack, countTracks, countMatchingTracks

# About us
about_us_cache = {"last_updated": time.time()}
CONFIG_DATA = load_yaml("config.yaml")
tokens = load_yaml("tokens.yaml")
PROJECT_ID = tokens['gitlab']['id']
PROJECT_ACCESS_TOKEN = tokens['gitlab']['token']

def update_info() -> None:
    """Update the about us page if needed.
    Otherwise use the data in the cache.
    Reloads info every 5 minutes.
    """
    total_unit_tests = 0
    for person, info in CONFIG_DATA['about_us'].items():
        if person == "project":
            continue
        total_unit_tests += info['unit_tests']

    if(time.time() - (about_us_cache['last_updated']) > 300.0
            or len(about_us_cache) == 1):
        cd, t = get_gl_stats(PROJECT_ID, PROJECT_ACCESS_TOKEN)
        about_us_cache['contributor_data'] = cd
        about_us_cache['totals'] = t
        about_us_cache['totals']['total_tests'] = total_unit_tests
        about_us_cache['last_updated'] = time.time()

@app.route('/')
def index() -> str:
    """Landing page for the app

    Returns:
        str: HTML Template
    """
    return render_template('index.html')


@app.route('/about-us/', methods=['GET'])
def about_us() -> Response:
    """About us page with statistics.
    Page contains cards with links to Postman, Gitlab, and user statistics.

    Returns:
        Response: JSON Response Object with about us data
    """
    try:
        update_info()

        return jsonify({
            'member_data': about_us_cache['contributor_data'],
            'totals': about_us_cache['totals'],
            'yaml_data': CONFIG_DATA['about_us']
        })
    except Exception as exc:
        return render_template('400.html', data=str(exc))

@app.route('/api/artists', methods=['GET'])
def search_artist_json() -> Response:
    """API endpoint for multiple artists. Arguments that can be provided
    are: limit, offset, sorting (attribute to sort by, and ascending or
    descending), and search terms.

    Returns:
        Response: JSON Response Object containing multiple artists
    """
    try:
        limit = int(request.args.get('limit', 10))
        offset = int(request.args.get('offset', 0))
        sort_by = str(request.args.get('sort_by', 'ASC'))
        sort_by_attr = str(request.args.get('sort_by_attribute', 'name'))
        search = str(request.args.get('search', ''))
        count = countArtists() if search == '' else countMatchingArtists(search)

        if (limit <= 0):
            raise ValueError('Invalid value for limit')
        total_pages = (count + limit - 1) // limit
        current_page = (offset // limit) + 1
        next_page = current_page == None if current_page == total_pages else current_page + 1
        prev_page = current_page == None if current_page == 1 else current_page - 1

        artist_data = {}
        artist_data['data'] = getArtists(limit=limit, offset=offset,
                                         sort_by=sort_by, sort_by_attribute=sort_by_attr, search=search)
        artist_data['pagination'] = {
            'total_entries':    count,
            'current_page':     current_page,
            'total_pages':      total_pages,
            'next_page':        next_page,
            'prev_page':        prev_page
        }
    except Exception as exc:
        return render_template('400.html', data=str(exc))
    return jsonify(artist_data)

@app.route('/api/artist/<id>', methods=['GET'])
def artist_model_json(id: str) -> Response:
    """Returns JSON of the artist with a matching id

    Args:
        id (str): ID of the artist

    Returns:
        Response: JSON Response Object of multiple artists
    """
    try:
        artist = getOneArtist(id)
    except Exception as exc:
        return render_template('400.html', data=str(exc))
    return jsonify(artist)

@app.route('/api/albums', methods=['GET'])
def search_album_json() -> Response:
    """API Endpoint for multiple albums. Arguments that may be provided are
    limit, offset, sorting (attribute and ascending/descending), and search
    terms.

    Returns:
        Response: JSON Response Object containing multiple albums
    """
    try:
        limit = int(request.args.get('limit', 10))
        offset = int(request.args.get('offset', 0))
        sort_by = str(request.args.get('sort_by', 'ASC'))
        sort_by_attr = str(request.args.get('sort_by_attribute', 'name'))
        search = str(request.args.get('search', ''))
        count = countAlbums() if search == '' else countMatchingAlbums(search)

        if (limit <= 0):
            raise ValueError('Invalid value for limit')
        total_pages = (count + limit - 1) // limit
        current_page = (offset // limit) + 1
        next_page = current_page == None if current_page == total_pages else current_page + 1
        prev_page = current_page == None if current_page == 1 else current_page - 1

        album_data = {}
        album_data['data'] = getAlbums(limit=limit, offset=offset,
                                       sort_by=sort_by, sort_by_attribute=sort_by_attr, search=search)
        album_data['pagination'] = {
            'total_entries':    count,
            'current_page':     current_page,
            'total_pages':      total_pages,
            'next_page':        next_page,
            'prev_page':        prev_page
        }
    except Exception as exc:
        return render_template('400.html', data=str(exc))
    return jsonify(album_data)

@app.route('/api/album/<id>', methods=["GET"])
def album_model_json(id: str) -> Response:
    """Returns JSON of the album that matches the id.

    Args:
        id (str): ID of the album.

    Returns:
        Response: JSON Response Object of the Album.
    """
    try:
        album = getOneAlbum(id)
    except Exception as exc:
        return render_template('400.html', data=str(exc))
    return jsonify(album)

@app.route('/api/tracks', methods=["GET"])
def search_track_json() -> Response:
    """API Endpoint for multiple tracks. Arguments that may be provided are
    limit, offset, sorting (attribute and ascending/descending), and search
    terms.

    Returns:
        Response: JSON Response Object containing multiple tracks
    """
    try:
        limit = int(request.args.get('limit', 10))
        offset = int(request.args.get('offset', 0))
        sort_by = str(request.args.get('sort_by', 'ASC'))
        sort_by_attr = str(request.args.get('sort_by_attribute', 'name'))
        search = str(request.args.get('search', ''))

        count = countTracks() if search == '' else countMatchingTracks(search)

        if (limit <= 0):
            raise ValueError('Invalid value for limit')
        total_pages = (count + limit - 1) // limit
        current_page = (offset // limit) + 1
        next_page = current_page == None if current_page == total_pages else current_page + 1
        prev_page = current_page == None if current_page == 1 else current_page - 1

        track_data = {}
        track_data['data'] = getTracks(limit=limit, offset=offset,
                                       sort_by=sort_by, sort_by_attribute=sort_by_attr, search=search)
        track_data['pagination'] = {
            'total_entries':    count,
            'current_page':     current_page,
            'total_pages':      total_pages,
            'next_page':        next_page,
            'prev_page':        prev_page
        }
    except Exception as exc:
        return render_template('400.html', data=str(exc))
    return jsonify(track_data)

@app.route('/api/track/<id>', methods=["GET"])
def track_model_json(id: str) -> Response:
    """Returns JSON of the track that matches the id.

    Args:
        id (str): ID of the track.

    Returns:
        Response: JSON Response Object of the Track.
    """
    try:
        track = getOneTrack(id)
    except Exception as exc:
        return render_template('400.html', data=str(exc))
    return jsonify(track)

@app.route('/dev/')
def developer_page() -> Response:
    """Developer only page. Shows our config file and any other parameters
    we might want to see during runtime.

    Goal is to debug errors we see.

    Returns:
        Response: JSON Response Object
    """
    return jsonify(CONFIG_DATA)

if __name__ == "__main__":
    # Debug if in dev mode
    app.debug = load_yaml("config.yaml")['developer_mode']

    # Begin the app
    app.run(host = '0.0.0.0', port = 8080)
