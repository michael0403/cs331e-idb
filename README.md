# cs331e-idb (Group 8)

## Table of Contents
1. [Links](#links)
2. [Setup](#setup)
    * [Venv Setup](#venv-instructions)
    * [Conda Setup](#conda-instructions)
3. [Git Workflow](#git-workflow)
4. [Git Commands](#git-commands)


## Links
* [Project details page](https://www.cs.utexas.edu/~fares/cs331es24/CS%20373_files/projects/IDB1.html)
* [Wiki Page](https://gitlab.com/michael0403/cs331e-idb/-/wikis/home)


## Setup
Create a virtual environment with python 3.8.10

Create a copy of `tokens-template.yaml` and rename it as `tokens.yaml`. This is
required for the App to work properly when communicating with external API's.

Create a `.env` file inside of `frontend/`. Then add the following line, using
the correct url for the Flask server.
```
REACT_APP_API_HOST=http://localhost:8080
```


#### Venv Instructions
In order for VirtualENV to create an environemnt with the correct python version
 you need to install python 3.8.10 first.\
Then create an environment using that specific python version.

Create the environment:\
`cd cs331e-idb`\
`python3.8 -m venv .venv`

Activate the environment:\
`. .venv/Scripts/activate      # Windows`\
`source .venv/bin/activate     # Mac/Linux`

Install the required packages: `pip install -r requirements.txt`


#### Conda Instructions
Create the environment:\
`cd cs331e-idb`\
`conda create -n cs331e-idb python=3.8.10`

Activate the environment:\
`conda activate cs331e-idb`

Install the required packages: `pip install -r requirements.txt`


## Git Workflow
We will be using 3 branches. `feature-branch` -> `dev` -> `main`
* The `main` branch is the deployed code on GCP.
* The `dev` branch is the working branch. This branch checks the code to make
sure it is working
* The `feature-branch` is the branch you make when you are working on new code.

Steps for a feature/thing to add or fix.
1. Create an issue for the feature.
2. Create a branch from `dev` and give it a relevant name.
3. Work on the code, make all the commits, and push to the `<feature-branch>`.
4. When your changes look good create a merge request from `<feature-branch>` to
`dev`. Once there is a merge request, GitLab will check for conflicts, unit
tests, and other members can review the code and approve.
5. Once the merge is approved, delete the `<feature-branch>` and close the issue.
6. After all the features are added to `dev` and the app is running, we can
push to `main` and add a tag. The version on `main` will be deployed to GCP.

__Example__
1. Create `Issue 0: Add a search bar to a page`.
2. Create a branch called `add-search-bar`.
3. Code and commit. Push commit A, commit B, and commit C to `add-search-bar`.
4. Create a merge request from `add-search-bar` -> `dev`.
    - Code review by rest of team.
5. Delete branch `add-search-bar` and close `Issue 0`.
6. Merge `dev` into `main` and give it a tag `V1.0` for phase 1.
    - Only merge into main when we have a lot of changes or need to deploy.


## Git Commands
* Check status and see what branch you are on: `git status`

* See existing branches: `git branch -a`


* See the git history of commits:
    * Normal Log: `git log`
    * Oneline per commit: `git log --oneline`

* Get changes from GitLab
    * Get all changes and delete non-existent branches: `git pull --all --prune`
    * Get changes from a specific branch: `git pull origin <branch-name>`

* Checkout a branch: `git checkout <branch-name>`

* Create a feature-branch from dev:
    * `git checkout dev`
    * `git checkout -b <feature-branch-name>`
* Add changes to files: `git add <file-name>`

* Commit changes to git history: `git commit -m "<commit-message>"`
    * Keep the message concise and meaningful. 

* Push commits to branch on GitLab:
    * Push a new branch that doesn't exist in GitLab:
    `git push -u origin <feature-branch-name>` 
    * Push to a branch that already exists:
        * `git push origin <feature-branch-name>`, or
        * `git push`

* Delete a branch
    * Delete locally: `git branch -d <branch-name>`
    * Delete from GitLab from computer: `git push -d origin <branch-name>`
