#!/usr/bin/env python3
"""
File: test.py
Authors: 
    Steven Ortega

Run all of the test files
"""
import unittest
import test_db
import test_api

from create_db import setup_db

if __name__ == '__main__':
    # setup the db once for all tests in dev mode
    setup_db(dev=True)
    
    # create test suite
    testsuite = unittest.TestSuite()

    # load the tests
    testsuite.addTest(unittest.defaultTestLoader.loadTestsFromModule(test_db))
    testsuite.addTest(unittest.defaultTestLoader.loadTestsFromModule(test_api))

    # execute the tests
    unittest.TextTestRunner().run(testsuite)
