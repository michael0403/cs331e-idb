#!/usr/bin/env python3
"""
File: test_db.py
Authors: 
    Steven Ortega

Tests database model and functions
"""
import unittest

from models import Artist, Track, Album, Artist_Genre, drops, releases
from create_db import setup_db

class TestDatabase(unittest.TestCase):
    """Test database api functionality"""

    # uncomment if running module on it's own
    # @classmethod
    # def setUpClass(cls):
    #     """Setup the database with developer_data to test the api calls
    #     Author: Steven
    #     """
    #     setup_db(dev=True)

    def test_database_creation(self):
        """Ensure the database is not empty
        Author: Steven
        """
        self.assertNotEqual(Album.query.count(), 0)
        self.assertNotEqual(Artist.query.count(), 0)
        self.assertNotEqual(Track.query.count(), 0)

    def test_insert_artist(self):
        pass

    def test_delete_artist(self):
        pass

    def test_get_artist(self):
        """Test that an Artist table entry is correct
        Author: Steven
        """
        trk = Track.query.first_or_404()
        self.assertIsNotNone(trk)
        trk = trk.serialize()
        self.assertIsNotNone(trk['id'])
        self.assertIsNotNone(trk['name'])
        self.assertIsNotNone(trk['popularity'])
        self.assertIsNotNone(trk['duration'])
        self.assertIsNotNone(trk['album_id'])
        self.assertIn('preview_url', trk)

    def test_insert_album(self):
        pass

    def test_delete_album(self):
        pass

    def test_get_album(self):
        pass

    def test_insert_track(self):
        pass

    def test_delete_track(self):
        pass

    def test_get_track(self):
        pass

if __name__ == "__main__":
    unittest.main()
