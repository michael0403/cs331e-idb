status:
	git status

test: test.py main.py
	coverage run --branch test.py > test-report.tmp 2>&1
	coverage report -m >> test-report.tmp
	cat test-report.tmp

IDB3.log:
	git log > IDB3.log

models.html: models.py
	pydoc3 -w models

test_api.html: test_api.py
	pydoc3 -w test_api

test_db.html: test_db.py
	pydoc3 -w test_db

main.html: main.py
	pydoc3 -w main


clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  test-report.tmp
	rm -rf __pycache__
	rm IDB3.log
