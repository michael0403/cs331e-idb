#!/usr/bin/env python3

import os
import sys

# Get access to scripts module
current_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(current_dir)
sys.path.append(parent_dir)

from flask_cors import CORS
from scripts.load_yaml import load_yaml
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# initializing Flask app 
app = Flask(__name__)
CORS(app)

app.app_context().push()

# Extract the database information
db_config = load_yaml('config.yaml')['database']
PUBLIC_IP_ADDRESS = db_config['ip_address']
DBNAME = db_config['name']
try:
    f = open('tokens.yaml', 'r')
    db_access = load_yaml('tokens.yaml')['database']
    USER = db_access['user']
    PASSWORD = db_access['password']
except FileNotFoundError:
    print('tokens.yaml not found, using default DB login')
    USER = 'postgres'
    PASSWORD = 'password'


print(f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')

# Configuration 
app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)

# ------------
# Junction Tables
# ------------
drops = db.Table('drops',
    db.Column('artist_id', db.String(40), db.ForeignKey('artist.id')),
    db.Column('track_id', db.String(40), db.ForeignKey('track.id'))
    )

releases = db.Table('releases',
    db.Column('artist_id', db.String(40), db.ForeignKey('artist.id')),
    db.Column('album_id', db.String(40), db.ForeignKey('album.id'))
    )

# ------------
# Artist
# ------------
class Artist(db.Model):
    """
    Artist class has four attrbiutes 
    id
    name
    img_url
    popularity
    """
    __tablename__ = 'artist'
    
    id = db.Column(db.String(40), nullable = False, primary_key = True)
    name = db.Column(db.String(50), nullable = False)
    img_url = db.Column(db.String(100))
    popularity = db.Column(db.Integer)

    # relationships
    genres = db.relationship('Artist_Genre', backref = 'artist')
    tracks = db.relationship('Track', secondary = 'drops', backref = 'dropped_by')
    albums = db.relationship('Album', secondary = 'releases', backref = 'released_by')
    
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id, 
            'name': self.name,
            'image': self.img_url,
            'popularity': self.popularity
            }
    

class Artist_Genre(db.Model):
    """
    id
    genre
    """
    __tablename__ = 'artist_genre'
    
    id = db.Column(db.String(40), db.ForeignKey('artist.id'), nullable = False, primary_key = True)
    genre = db.Column(db.String(40), nullable = False, primary_key = True)

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id, 
            'genre': self.genre
        }

# ------------
# Album
# ------------
class Album(db.Model):
    """
    Album class has six attrbiutes 
    id
    name
    img_url
    release_date
    label
    popularity
    """
    __tablename__ = 'album'
    
    id = db.Column(db.String(40), nullable = False, primary_key = True)
    name = db.Column(db.String(130), nullable = False)
    img_url = db.Column(db.String(100))
    release_date = db.Column(db.DateTime)
    label = db.Column(db.String(100))
    popularity = db.Column(db.Integer)
    
    # relationships
    tracks = db.relationship('Track', backref = 'tracks')

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id, 
            'name': self.name,
            'image': self.img_url,
            'release_date': self.release_date,
            'label': self.label,
            'popularity': self.popularity
            }

# ------------
# Track
# ------------
class Track(db.Model):
    """
    Track class has four attrbiutes 
    id
    name
    duration_ms
    popularity
    preview_url
    album_id
    """
    __tablename__ = 'track'

    id = db.Column(db.String(40), nullable = False, primary_key = True)
    name = db.Column(db.String(130), nullable = False)
    preview_url = db.Column(db.String(107))
    popularity = db.Column(db.Integer)
    duration_ms = db.Column(db.Integer)

    # relationships
    album_id = db.Column(db.String(40), db.ForeignKey('album.id'))

    # ------------
    # serialize
    # ------------
    def serialize(self):
        """
        returns a dictionary
        """
        return {
            'id': self.id, 
            'name': self.name,
            'popularity': self.popularity,
            'preview_url': self.preview_url,
            'duration': self.duration_ms,
            'album_id': self.album_id
            }