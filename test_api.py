#!/usr/bin/env python3
"""
File: test_api.py
Authors: 
    Steven Ortega

Tests api functionality
"""
import unittest
import pprint
from create_db import setup_db
from db.album_api import countAlbums, getAlbums, getOneAlbum
from db.artist_api import countArtists, getArtists, getOneArtist
from db.track_api import countTracks, getOneTrack, getTracks
from scripts.get_gitlab_statistics import get_gitlab_statistics
from models import Artist, Track, Album, db

class TestDatabaseAPI(unittest.TestCase):
    """Test database api functionality"""

    # uncomment if running module on it's own
    # @classmethod
    # def setUpClass(cls):
    #     """Setup the database with developer_data to test the api calls
    #     Author: Steven
    #     """
    #     setup_db(dev=True)

    #--------
    # Gitlab
    #--------
    def test_get_gitlab_statistics(self):
        """Tests that the gitlab statistics function connects with the API
        and outputs valid data.

        Author: Steven
        """
        # Token only gives access for this unit test
        test_project = 54360564
        token = 'glpat-PuQQHeGaRbxrxthtzNdy'
        r = get_gitlab_statistics(test_project, token)
        self.assertIsInstance(r, tuple)
        self.assertIsNot(r[0], {})
        self.assertIsNot(r[1], {})
    
    #---------
    # Artists
    #---------
    def test_get_artist_api(self):
        """Test we can get multiple artists
        Author: Steven
        """
        # Test getting one artist
        testcase = getArtists(1)
        self.assertEqual(len(testcase), 1)
        self.assertIsNotNone(testcase[0]['id'])
        self.assertIsNotNone(testcase[0]['name'])

        # Test getting three artists
        testcase2 = getArtists(3)
        self.assertEqual(len(testcase2), 3)
        self.assertIsNotNone(testcase2[0]['id'])
        self.assertIsNotNone(testcase2[1]['name'])
        self.assertIsNotNone(testcase2[2])

        # Test getting unspecified number of artists
        testcase2 = getArtists()
        self.assertEqual(len(testcase2), countArtists())

        # Test invalid parameters
        with self.assertRaises(ValueError):
            getArtists(limit=-1)
            getArtists(limit=0)
            getArtists(offset=countArtists()+1)
            getArtists(offset=countArtists())
            getArtists(sort_by='asc')
            getArtists(sort_by_attribute='test')

        # Test edge cases
        self.assertEqual(getArtists()[-1], getArtists(offset=-1)[0])
        self.assertEqual(3, len(getArtists(offset=-3)))
        

    def test_one_artist(self):
        """Test that we can get one artist
        Author: Steven
        """
        testartists = getArtists(3)
        test_art_id = testartists[2]['id']
        testcase = getOneArtist(test_art_id)
        self.assertEqual(testcase['id'], test_art_id)
        self.assertIsNotNone(testcase['genres'])
        self.assertIsNotNone(testcase['albums'])
    
    def test_count_artists(self):
        """Test the count_artists function matches # rows in DB
        Author: Steven
        """
        num_rows = Artist.query.count()
        self.assertEqual(countArtists(), num_rows)

    def test_artist_sorting(self):
        """Test the sorting feature of getArtists
        Author: Steven
        """
        t1, t2, t3 = getArtists(3, sort_by_attribute='popularity')
        first = db.session.query(Artist).where(Artist.id==t1['id']).first()
        second = db.session.query(Artist).where(Artist.id==t2['id']).first()
        third = db.session.query(Artist).where(Artist.id==t3['id']).first()

        self.assertGreaterEqual(second.popularity, first.popularity)
        self.assertGreaterEqual(third.popularity, second.popularity)
        self.assertLessEqual(first.popularity, third.popularity)   

    #---------
    # Tracks
    #---------
    def test_get_track_api(self):
        """Test we can get multiple tracks
        Author: Steven
        """
        # Test getting one artist
        testcase = getTracks(1)
        self.assertEqual(len(testcase), 1)
        self.assertIsNotNone(testcase[0]['id'])
        self.assertIsNotNone(testcase[0]['name'])

        # Test getting three artists
        testcase2 = getTracks(3)
        self.assertEqual(len(testcase2), 3)
        self.assertIsNotNone(testcase2[0]['id'])
        self.assertIsNotNone(testcase2[1]['name'])
        self.assertIsNotNone(testcase2[2])

        # Test getting unspecified number of artists
        testcase2 = getTracks()
        self.assertEqual(len(testcase2), countTracks())

        # Test invalid parameters
        with self.assertRaises(ValueError):
            getTracks(limit=-1)
            getTracks(limit=0)
            getTracks(offset=countTracks()+1)
            getTracks(offset=countTracks())
            getTracks(sort_by='asc')
            getTracks(sort_by_attribute='test')


        # Test edge cases
        self.assertEqual(getTracks()[-1], getTracks(offset=-1)[0])
        self.assertEqual(3, len(getTracks(offset=-3)))

    def test_one_track(self):
        """Test that we can get one track
        Author: Steven
        """
        testtracks = getTracks(3)
        test_trk_id = testtracks[2]['id']
        testcase = getOneTrack(test_trk_id)
        self.assertEqual(testcase['id'], test_trk_id)
        self.assertIsNotNone(testcase['artists'])
        self.assertIsNotNone(testcase['album_name'])
        self.assertIsNotNone(testcase['duration'])

    def test_count_tracks(self):
        """Test the count_tracks function matches # rows in DB
        Author: Steven
        """
        num_rows = Track.query.count()
        self.assertEqual(countTracks(), num_rows)

    def test_track_sorting(self):
        """Test the sorting feature of getTracks.
        Author: Steven
        """
        t1, t2, t3 = getTracks(3, sort_by_attribute='duration_ms')
        first = db.session.query(Track).where(Track.id==t1['id']).first()
        second = db.session.query(Track).where(Track.id==t2['id']).first()
        third = db.session.query(Track).where(Track.id==t3['id']).first()

        self.assertGreaterEqual(second.duration_ms, first.duration_ms)
        self.assertGreaterEqual(third.duration_ms, second.duration_ms)
        self.assertLessEqual(first.duration_ms, third.duration_ms)

    #---------
    # Albums
    #---------
    def test_get_album_api(self):
        """Test we can get multiple albums
        Author: Saurelle
        """
        # Test getting one album
        testcase = getAlbums(1)
        self.assertEqual(len(testcase), 1)
        self.assertIsNotNone(testcase[0]['id'])
        self.assertIsNotNone(testcase[0]['name'])

        # Test getting three albums
        testcase2 = getAlbums(3)
        self.assertEqual(len(testcase2), 3)
        self.assertIsNotNone(testcase2[0]['id'])
        self.assertIsNotNone(testcase2[1]['name'])
        self.assertIsNotNone(testcase2[2])

        # Test getting unspecified number of albums
        testcase2 = getAlbums()
        self.assertEqual(len(testcase2), countAlbums())

        # Test invalid parameters
        with self.assertRaises(ValueError):
            getAlbums(limit=-1)
            getAlbums(limit=0)
            getAlbums(offset=countAlbums()+1)
            getAlbums(offset=countAlbums())
            getAlbums(sort_by='asc')

        # Test edge cases
        self.assertEqual(getAlbums()[-1], getAlbums(offset=-1)[0])
        self.assertEqual(3, len(getAlbums(offset=-3)))

        # Test invalid parameters
        with self.assertRaises(ValueError):
            getAlbums(limit=-1)
            getAlbums(limit=0)
            getAlbums(offset=countAlbums()+1)
            getAlbums(offset=countAlbums())
            getAlbums(sort_by='asc')
            getAlbums(sort_by_attribute='test')

        # Test edge cases
        self.assertEqual(getAlbums()[-1], getAlbums(offset=-1)[0])
        self.assertEqual(3, len(getAlbums(offset=-3)))

    def test_one_album(self):
        """Test that we can get one album
        Author: Saurelle
        """
        testtracks = getAlbums(3)
        test_trk_id = testtracks[2]['id']
        testcase = getOneAlbum(test_trk_id)
        self.assertEqual(testcase['id'], test_trk_id)
        self.assertIsNotNone(testcase['artists'])
        self.assertIsNotNone(testcase['list_of_tracks'])
        self.assertIsNotNone(testcase['image'])

    def test_count_albums(self):
        """Test the count_albums function matches # of rows in DV
        Author: Saurelle
        """
        num_rows = Album.query.count()
        self.assertEqual(countAlbums(), num_rows)

    def test_album_sorting(self):
        """Test the sorting feature of getAlbums
        Author: Steven
        """
        t1, t2, t3 = getAlbums(3, sort_by_attribute='release_date')
        first = db.session.query(Album).where(Album.id==t1['id']).first()
        second = db.session.query(Album).where(Album.id==t2['id']).first()
        third = db.session.query(Album).where(Album.id==t3['id']).first()

        self.assertGreaterEqual(second.release_date, first.release_date)
        self.assertGreaterEqual(third.release_date, second.release_date)
        self.assertLessEqual(first.release_date, third.release_date)
        

if __name__ == "__main__":
    # unittest.main()
    artist_tests = ['test_get_artist_api', 'test_one_artist', 'test_count_artists', 'test_artist_sorting']
    track_tests = ['test_get_track_api', 'test_one_track', 'test_count_tracks', 'test_track_sorting']
    album_tests = ['test_get_album_api', 'test_one_album', 'test_count_albums', 'test_album_sorting']

    # create test suite
    artist_testsuite = unittest.TestSuite()
    track_testsuite = unittest.TestSuite()
    album_testsuite = unittest.TestSuite()

    # load the tests
    for test in artist_tests:
        artist_testsuite.addTest(TestDatabaseAPI(test))

    for test in track_tests:
        track_testsuite.addTest(TestDatabaseAPI(test))

    for test in album_tests:
        album_testsuite.addTest(TestDatabaseAPI(test))

    # execute the tests
    print('Testing Artist API')
    unittest.TextTestRunner().run(artist_testsuite)
    print('Testing Track API')
    unittest.TextTestRunner().run(track_testsuite)
    print('Testing Album API')
    unittest.TextTestRunner().run(album_testsuite)