#!/usr/bin/env python3
"""
File: album_api.py
Authors: 
    Saurelle Ngaintse Jiotsa
    Steven Ortega
    Rory James

API interface between database and user for the Album table
"""
from models import db, Artist, Album, Track, releases
from typing import Dict, List
from sqlalchemy import and_

def getAlbums(limit: int=None, offset: int=None, sort_by: str='ASC',
              sort_by_attribute: str='name', search: str='') -> List[Dict]:
    """Returns all the album objects in the DB

    Args:
        limit (int, optional): number of albums to return. Defaults to None.
        offset (int, optional): number of rows to skip. Defaults to None.
        sort_by (str, optional): Sort by ASC or DESC. Defaults to 'ASC'.
        sort_by_attribute (str, optional): Attribute to sort by. Defaults to 'name'.

    Raises:
        ValueError: Raised if sorting is not one of (ASC, DESC) or a valid attribute.

    Returns:
        List[Dict]: Returns a list of Album dictionaries
    """
    if (sort_by_attribute not in Album.__dict__.keys()):
        raise ValueError('Invalid sort by attribute')

    if (limit is not None and limit < 1):
        raise ValueError('Invalid value for limit')
    
    if (offset is not None and offset >= countAlbums()):
        raise ValueError('Offset is greater than number of entries in DB')
        
    if (offset is not None and offset < 0):
        offset = countAlbums() + offset

    sort_attr = getattr(Album, sort_by_attribute)
    if sort_by == 'ASC':
        order_param = sort_attr.asc()
    elif sort_by == 'DESC':
        order_param = sort_attr.desc()
    else:
        raise ValueError('sort_by key is not one of (ASC, DESC); sort_by value: ' + sort_by)
    
    if search == '':
        db_albums = Album.query.order_by(order_param).offset(offset) \
            .limit(limit).all()
    else:
        terms = search.split(' ')
        likes = []
        for term in terms:
            likes.append(Album.name.like(f'%{term}%'))
        db_albums = Album.query.filter(and_(*likes)).order_by(order_param) \
            .offset(offset).limit(limit).all()


    album_list = [
        {
            'id': item.id,
            'name': item.name,
            'image': item.img_url,
            'release_date': item.release_date
        } for item in db_albums
    ]
    return album_list

def countAlbums() -> int:
    """Returns the number of Albums in the DB

    Returns:
        int: number of rows in the Albums table
    """
    return (db.session.query(Album).count())
    
def getOneAlbum(id: str) -> str:
    """Returns an Album object from the DB

    Args:
        id (str): ID of the album entry

    Returns:
        Dict: An album dictionary with relevant fields
    """
    album = db.session.query(Album).filter(Album.id==id).first_or_404()
    tracks =  db.session.query(Track).filter(Track.album_id==id).all()
    artists = db.session.query(Artist).join(releases).join(Album).filter(Album.id==id).all()
    
    single_album = {
        'id': album.id, 
        'name': album.name, 
        'image': album.img_url, 
        'release_date':str(album.release_date), 
        'label':album.label, 
        'popularity':album.popularity,
        'artists':[
            {
                'artist_id': artist_item.id,
                'artist_name':artist_item.name
            } for artist_item in artists
        ],
        'list_of_tracks':[
            {
                'track_id': track_item.id,
                'name_track':track_item.name
            } for track_item in tracks
        ]
    }
    return single_album

def countMatchingAlbums(search: str) -> int:
    """Returns number of albums in DB matching a search query

    Args:
        search: search terms

    Returns:
        int: number of rows in Album table matching query
    """
    terms = search.split(' ')
    likes = []
    for term in terms:
        likes.append(Album.name.like(f'%{term}%'))
    return Album.query.filter(and_(*likes)).count()