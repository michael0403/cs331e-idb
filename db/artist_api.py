#!/usr/bin/env python3
"""
File: artist_api.py
Authors: 
    Steven Ortega
    Rory James

API interface between database and user for the Artist table
"""
from typing import Dict, List
from models import Album, Artist, Artist_Genre, releases
from sqlalchemy import and_


def getArtists(limit: int=None, offset: int=None, sort_by: str='ASC',
               sort_by_attribute: str='name', search: str='') -> List[Dict]:
    """Returns all the artists objects in the DB

    Args:
        limit (int, optional): number of artists to return. Defaults to None.
        offset (int, optional): number of rows to skip. Defaults to None.
        sort_by (str, optional): Sort by ASC or DESC. Defaults to 'ASC'.
        sort_by_attribute (str, optional): Attribute to sort by. Defaults to 'name'.

    Raises:
        ValueError: Raised if sorting is not one of (ASC, DESC) or a valid attribute.

    Returns:
        List[Dict]: Returns a list of Artist dictionaries
    """
    if (sort_by_attribute not in Artist.__dict__.keys()):
        raise ValueError('Invalid sort by attribute')

    if (limit is not None and limit < 1):
        raise ValueError('Invalid value for limit')
    
    if (offset is not None and offset >= countArtists()):
        raise ValueError('Offset is greater than number of entries in DB')
    
    if (offset is not None and offset < 0):
        offset = countArtists() + offset

    sort_attr = getattr(Artist, sort_by_attribute)
    if sort_by == 'ASC':
        order_param = sort_attr.asc()
    elif sort_by == 'DESC':
        order_param = sort_attr.desc()
    else:
        raise ValueError('sort_by key is not one of (ASC, DESC); sort_by value: ' + sort_by)
    
    if search == '':
        db_artists = Artist.query.order_by(order_param).offset(offset) \
            .limit(limit).all()
    else:
        terms = search.split(' ')
        likes = []
        for term in terms:
            likes.append(Artist.name.like(f'%{term}%'))
        db_artists = Artist.query.filter(and_(*likes)).order_by(order_param) \
            .offset(offset).limit(limit).all()

    artists = []
    for artist in db_artists:
        artists.append(
            {
                "id": artist.id,
                "name": artist.name,
                "image": artist.img_url,
                "popularity": artist.popularity
            }
        )
    return artists

def getOneArtist(id: str) -> Dict:
    """Returns an Artist object from the DB

    Args:
        id (str): ID of the artist entry

    Returns:
        Dict: An artist dictionary with relevant fields
    """
    artist = Artist.query.filter(Artist.id==id).first_or_404().serialize()

    # get genres
    db_genres = Artist_Genre.query.filter_by(id=id).all()
    genres = [genre.genre for genre in db_genres]
    
    # get albums
    db_albums = Album.query.join(releases).join(Artist).filter(Artist.id==id).all()
    albums = []
    for album in db_albums:
        albums.append(
            {
                'album_id': album.id,
                'album_name': album.name,
                'album_image': album.img_url
            }
        )
    
    # format the artist
    artist['genres'] = genres
    artist['albums'] = albums

    return artist

def countArtists() -> int:
    """Returns the number of Artists in the DB

    Returns:
        int: number of rows in the Artist table
    """
    return Artist.query.count()

def countMatchingArtists(search: str) -> int:
    """Returns number of artists in DB matching a search query

    Args:
        search: search terms

    Returns:
        int: number of rows in Artist table matching query
    """
    terms = search.split(' ')
    likes = []
    for term in terms:
        likes.append(Artist.name.like(f'%{term}%'))
    return Artist.query.filter(and_(*likes)).count()