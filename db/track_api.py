#!/usr/bin/env python3
"""
File: track_api.py
Authors: 
    Steven Ortega
    Rory James

API interface between database and user for the Track table
"""

from models import Artist, Album, drops, Track
from typing import List, Dict
from sqlalchemy import and_


def getTracks(limit: int=None, offset: int=None, sort_by: str='ASC',
              sort_by_attribute: str='name', search: str='') -> List[Dict]:
    """Returns all the track objects in the DB

    Args:
        limit (int, optional): number of tracks to return. Defaults to None.
        offset (int, optional): number of rows to skip. Defaults to None.
        sort_by (str, optional): Sort by ASC or DESC. Defaults to 'ASC'.
        sort_by_attribute (str, optional): Attribute to sort by. Defaults to 'name'.
        
    Raises:
        ValueError: Raised if sorting is not one of (ASC, DESC) or a valid attribute.

    Returns:
        List[Dict]: Returns a list of Track dictionaries
    """
    if (sort_by_attribute != 'rel' and not sort_by_attribute in Track.__dict__.keys()):
        raise ValueError('Invalid sort by attribute')

    if (limit is not None and limit < 1):
        raise ValueError('Invalid value for limit')
    
    if (offset is not None and offset >= countTracks()):
        raise ValueError('Offset is greater than number of entries in DB')

    if (offset is not None and offset < 0):
        offset = countTracks() + offset

    sort_attr = getattr(Track, sort_by_attribute)
    if sort_by == 'ASC':
        order_param = sort_attr.asc()
    elif sort_by == 'DESC':
        order_param = sort_attr.desc()
    else:
        raise ValueError('sort_by key is not one of (ASC, DESC); sort_by value: ' + sort_by)
    
    if search == '':
        db_tracks = Track.query.order_by(order_param).offset(offset) \
            .limit(limit).all()
    else:
        terms = search.split(' ')
        likes = []
        for term in terms:
            likes.append(Track.name.like(f'%{term}%'))
        db_tracks = Track.query.filter(and_(*likes)).order_by(order_param) \
            .offset(offset).limit(limit).all()

    tracks = []
    for track in db_tracks:
        img = Album.query.filter(Album.id==track.album_id).first_or_404().img_url
        tracks.append(
            {
                "id": track.id,
                "name": track.name,
                "album_img_url": img
            }
        )
    return tracks

def getOneTrack(id: str) -> Dict:
    """Returns a Track object from the DB

    Args:
        id (str): ID of the track entry

    Returns:
        Dict: A Track dictionary with relevant fields
    """
    track = Track.query.filter(Track.id==id).first_or_404().serialize()
    album = Album.query.filter(Album.id==track['album_id']).first_or_404()

    # get the artits for the track
    artists = []
    db_artists = Artist.query.join(drops).join(Track).filter(Track.id==id).all()
    for artist in db_artists:
        artists.append(
            {
                'artist_id': artist.id,
                'artist_name': artist.name
            }
        )

    # format the track
    track['album_name'] = album.name
    track['album_img_url'] = album.img_url
    track['artists'] = artists

    return track

def countTracks() -> int:
    """Returns the number of tracks in the DB

    Returns:
        int: number of rows in the Track table
    """
    return Track.query.count()

def countMatchingTracks(search: str) -> int:
    """Returns number of tracks in DB matching a search query

    Args:
        search: search terms

    Returns:
        int: number of rows in Track table matching query
    """
    terms = search.split(' ')
    likes = []
    for term in terms:
        likes.append(Track.name.like(f'%{term}%'))
    return Track.query.filter(and_(*likes)).count()
