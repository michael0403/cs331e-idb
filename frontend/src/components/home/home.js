import React from 'react';
import './home.css';
import homePageImage from '../../images/home_page.png'; 

function HomePage() {
  const bgStyle = {
    backgroundImage: `url(${homePageImage})`,
    backgroundSize: 'cover',
    height: '100vh',
    backgroundPosition: 'center center'
  };

  return (
    <div className="bg-image" style={bgStyle}>
      <div className="grid-container">
        <h1 style={{ fontSize: '3em' }}>MusicMosaic</h1>
        <h2>Find your favorite music!</h2>
      </div>
    </div>
  );
}

export default HomePage;

