import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { useParams, Link } from 'react-router-dom'
import './tracks.css'

function TrackModel () {
  const { id } = useParams() // Use useParams to get the songID parameter from the URL
  const [track, setTrack] = useState({})

  useEffect(() => {
    ;(async () => {
      const data = await axios.get(
        `${process.env.REACT_APP_API_HOST}/api/track/${id}`
      )
      console.log(data.data)
      setTrack(data.data)
    })()
  }, [id])

  if (Object.keys(track).length === 0) {
    return <div>Track not found</div> // Fallback if track ID is not in the data.
  }

  return (
    <div className = 'container' id =  'track-info'>
      <div className = 'row'>
        <div className='col-4'>
          <img
            src={track.album_img_url}
            alt={`${track.name} album cover`}
            id = 'track-img'
          />
        </div>
        <div className='col-8'>
          <div class = "track-name">
          {track.name}
          </div>
          <h2>
            <b>Duration:</b> { Math.floor(track.duration/60000) } minutes { Math.floor(((track.duration/60000)%1)*60)} seconds
          </h2>
          <br></br>
          {track.preview_url ? (
          <div>
            <h2>Preview:</h2>
            <audio controls>
              <source src={track.preview_url} type='audio/mpeg' />
            </audio>
          </div>
          ) : (
            <h2>
              <b>Preview not available for this song</b>
            </h2>
          )}
          <br></br>
          <h2>
            <b>Popularity Score:</b> {track.popularity}/100
          </h2>
          <br></br>
          <h2>Artists:</h2>
          <ul>
            {track.artists.map(artist => (
              <li key={artist.artist_id}>
                <Link to={`/artists/${artist.artist_id}`} className='highlight'>
                  {artist.artist_name}
                </Link>
              </li>
            ))}
          </ul>
          <br />
          <h2>Album:</h2>
          <Link to={`/albums/${track.album_id}`} className='highlight'>
            {track.album_name}
          </Link>
        </div>
      </div>
      <br></br>
    </div>
  )
}

export default TrackModel
