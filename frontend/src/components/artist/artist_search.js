import React from 'react'
import './artist.css'
import axios from 'axios'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'; 
import ModelPagination from '../ModelPagination'

const PAGE_SIZE = 20;

function highlightText(text, highlight) {
  // Check if the highlight term is empty or consists only of spaces
  if (!highlight.trim()) {
    return <span>{text}</span>;
  }

  // Split the highlight string into individual words
  const terms = highlight.split(' ').filter(term => term.length > 0);
  // Join the terms into a single regex pattern with "or"
  const regexPattern = terms.join('|');
  // Create a RegExp object which is case-insensitive (i) and global (g)
  const regex = new RegExp(`(${regexPattern})`, 'gi');

  // Split the text into parts based on the regex and map over them
  const parts = text.split(regex);
  return <span>{parts.map((part, i) => 
    part.match(regex) ? <b key={i} style={{ backgroundColor: 'yellow' }}>{part}</b> : part
  )}</span>;
}




function ArtistSearch() {
  const [artists, setArtists] = useState([]);
  const [pageNum, setPageNum] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const navigate = useNavigate();
  const [sortOption, setSortOption] = useState('name');
  const [sortOrder, setSortOrder] = useState('ASC');
  const [sortDisplay, setSortDisplay] = useState('Name Ascending');
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    const fetchArtists = async () => {
      const queryParam = searchQuery ? `&search=${encodeURIComponent(searchQuery)}` : '';
      const api_data = await axios.get(
        `${process.env.REACT_APP_API_HOST}/api/artists?limit=${PAGE_SIZE}&offset=${pageNum * PAGE_SIZE - PAGE_SIZE}&sort_by_attribute=${sortOption}&sort_by=${sortOrder}${queryParam}`
      );
      setArtists(api_data.data.data);
      setTotalPages(api_data.data.pagination.total_pages);
    };
    fetchArtists();
  }, [pageNum, sortOption, sortOrder, searchQuery]);
  

  const handleSortChange = (option, order) => {
    setSortOption(option);
    setSortOrder(order);
    let displayText = option.charAt(0).toUpperCase() + option.slice(1);
    displayText += order === 'ASC' ? ' Ascending' : ' Descending';
    setSortDisplay(displayText);
  };

  return (
    <div className='container mt-5'>
      <div className="search-container my-3">
        <input
          type="text"
          placeholder="Search artists..."
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
          className="form-control"
          style={{ width: '40%' }}
        />
      </div>
      <div className="order-button">
        <button className="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Sort By: {sortDisplay}
        </button>
        <div className="dropdown-menu">
          <a className="dropdown-item" onClick={(e) => {e.preventDefault(); handleSortChange('name', 'ASC');}}>Name Ascending</a>
          <a className="dropdown-item" onClick={(e) => {e.preventDefault(); handleSortChange('name', 'DESC');}}>Name Descending</a>
          <a className="dropdown-item" onClick={(e) => {e.preventDefault(); handleSortChange('popularity', 'ASC');}}>Popularity Ascending</a>
          <a className="dropdown-item" onClick={(e) => {e.preventDefault(); handleSortChange('popularity', 'DESC');}}>Popularity Descending</a>
        </div>
      </div>
      <ModelPagination
        pageNum = {pageNum}
        totalPages ={totalPages}
        setPageNum={setPageNum}
      />
      <div className='row'>
        {artists.map(artist => (
          <div className='col-md-3 col-sm-6 mb-4' key={artist.id}>
            <div className='card'>
            <div
                className="img-container"
                onClick={() => navigate(`/artists/${artist.id}`)}
              >
                <img
                  src={artist.image}
                  className='card-img-top'
                  alt={artist.name}
                />
                <div className="overlay">
                  <div className="overlay-text">Learn More</div>
                </div>
              </div>
              <div className='card-body text-center'>
                <h5 className='card-title'>{highlightText(artist.name, searchQuery)}</h5>
                <p className='card-text'>Popularity: {artist.popularity}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
      <ModelPagination
        pageNum = {pageNum}
        totalPages ={totalPages}
        setPageNum={setPageNum}
      />
    </div>
  );
}

export default ArtistSearch
