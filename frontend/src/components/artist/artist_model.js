import React, { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom'
import './artist.css'
import axios from 'axios'

function ArtistModel () {
  const { id } = useParams()
  const [artist, setArtist] = useState(null)

  useEffect(() => {
    ;(async () => {
      const data = await axios.get(
        `${process.env.REACT_APP_API_HOST}/api/artist/${id}`
      )
      console.log(data.data)
      setArtist(data.data)
    })()
  }, [id])

  if (!artist) {
    return <div>Artist not found</div>
  }

  return (
    <div className='container' id = 'artist-info'>
      <div className='row'>
        <div class="col-4">
          <img 
            src={artist.image} 
            alt={artist.name} 
            id = 'artist-img'
          />
        </div>
        <div class="col-8">
          <div class = "artist-name">
          {artist.name}
          </div>
          <h2 className='mt-2'><b>Popularity Score:</b> {artist.popularity}/100</h2>
          <br></br>
          <h2><b>Genres</b></h2>
          <ul className='genre-list' id = 'genre-list'>
            {artist.genres.length > 0
              ? artist.genres.map((genre, index) => (
                <span id = "bubble" class="badge badge-pill badge-mine" key={index}>{genre}</span>
                ))
              : 'No genres available for this artist'}
          </ul>
          <br></br>
          <h2><b>Albums</b></h2>
          {artist.albums.length > 0
            ? artist.albums.map(album => (
                <div key={album.album_id} className='album-details'>
                  <Link to={`/albums/${album.album_id}`} className='highlight'>
                    {album.album_name}
                  </Link>
                  <br></br>
                  <img
                    src={album.album_image}
                    alt={album.album_name}
                    id='img-thumbnail'
                    className = "full-width-image"
                  />
                </div>
              ))
            : 'No albums available for this artist'}
        </div>
      </div>
      <br></br>
    </div>
  )
}

export default ArtistModel
