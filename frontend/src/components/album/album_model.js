import React, { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom'
import './album.css'
import axios from 'axios'

function AlbumModel () {
  const { id } = useParams() // Extract the album ID from the URL
  const [album, setAlbum] = useState({})

  useEffect(() => {
    ;(async () => {
      const data = await axios.get(
        `${process.env.REACT_APP_API_HOST}/api/album/${id}`
      )
      console.log(data.data)
      setAlbum(data.data)
    })()
  }, [id])

  // checks if album object is empty
  if (Object.keys(album).length === 0) {
    return <div>Album not found</div>
  }
  // IMPORTANT this will change when api is ready

  return (
    <div className='container' id = 'album-info'>
      <div className='row'>
        <div className = 'col-4'>
          <img
            src={album.image}
            title={album.name}
            alt='Album Cover'
            id ='album-img'
          />
        </div>
        <div className = 'col-8'>
          <div class = "album-name">
            {album.name}
          </div>
          <h2><b>Release Date:</b> {album.release_date}</h2>
          <br></br>
          <h2><b>Artists:</b></h2>
          {album.artists.map(artist => (
            <div key={artist.artist_id} className = 'artist-details'>
              <Link to={`/artists/${artist.artist_id}`} className = 'highlight'>
                {artist.artist_name}
              </Link>
            </div>
          ))}
          <br></br>
          <h2><b>List of Tracks:</b></h2>
          <ul class="list-group list-group-flush">
          {album.list_of_tracks.map(track => (
            <div key={track.track_id}>
              <li class="list-group-item bg-transparent">
              <Link to={`/tracks/${track.track_id}`} className = 'highlight'>{track.name_track}</Link>
              </li>
            </div>
          ))}
          </ul>
        </div>
      </div>
      <br></br>
    </div>
  )
}

export default AlbumModel
