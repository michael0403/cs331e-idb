import React, { useState, useEffect, useRef } from 'react';
import { Link} from 'react-router-dom';
import './navbar.css';
import { debounce } from 'lodash';
import axios from 'axios';

function Navbar() {
  const [isNavCollapsed, setIsNavCollapsed] = useState(true);
  const [searchQuery, setSearchQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [selectedModel, setSelectedModel] = useState('all');
  const searchResultsRef = useRef(null);

  const handleNavCollapse = () => setIsNavCollapsed(!isNavCollapsed);

  const handleSearchResultClick = () => {
    // clear results
    setSearchResults([]);
    setSearchQuery('');
  };


  const handleClickOutside = (event) => {
    if (searchResultsRef.current && !searchResultsRef.current.contains(event.target)) {
      setSearchResults([]); // If click outside, clear results
    }
  };

  const fetchSearchResults = debounce(async (query, model) => {
    try {
      const endpoints = {
        all: [
          { url: `${process.env.REACT_APP_API_HOST}/api/artists?search=${encodeURIComponent(query)}`, type: 'artist' },
          { url: `${process.env.REACT_APP_API_HOST}/api/tracks?search=${encodeURIComponent(query)}`, type: 'track' },
          { url: `${process.env.REACT_APP_API_HOST}/api/albums?search=${encodeURIComponent(query)}`, type: 'album' },
        ],
        artist: [{ url: `${process.env.REACT_APP_API_HOST}/api/artists?search=${encodeURIComponent(query)}`, type: 'artist' }],
        track: [{ url: `${process.env.REACT_APP_API_HOST}/api/tracks?search=${encodeURIComponent(query)}`, type: 'track' }],
        album: [{ url: `${process.env.REACT_APP_API_HOST}/api/albums?search=${encodeURIComponent(query)}`, type: 'album' }],
      };
  
      const requests = endpoints[model].map(endpoint =>
        axios.get(endpoint.url).then(response => ({
          data: response.data.data,
          type: endpoint.type
        }))
      );
  
      const responses = await Promise.all(requests);
      const combinedResults = responses.flatMap(response =>
        response.data.map(item => ({
          ...item,
          type: response.type // Assign type from response object
        }))
      );
  
      setSearchResults(combinedResults);
    } catch (error) {
      console.error('Error fetching search results:', error);
    }
  }, 300);
    // buffer so drop down results isnt often

  const handleSearchChange = (event) => {
    const query = event.target.value;
    setSearchQuery(query);
    if (query.length > 0) {
      fetchSearchResults(query, selectedModel);
    } else {
      setSearchResults([]);
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      fetchSearchResults.cancel();
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <nav className="navbar navbar-expand-lg navbar-dark">
      <Link className="navbar-brand" to="/">MusicMosaic</Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse" 
        data-target="#navbarSupportedContent" 
        aria-controls="navbarSupportedContent"
        aria-expanded={!isNavCollapsed}
        aria-label="Toggle navigation"
        onClick={handleNavCollapse}
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className={`${isNavCollapsed ? 'collapse' : ''} navbar-collapse`} id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/artists">Artists</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/albums">Albums</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/tracks">Songs</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/about-us">About Us</Link>
          </li>
        </ul>
        <div className="form-inline my-2 my-lg-0 navbar-right">
        <div className="input-group">
        <select
          className="custom-select"
          style={{ maxWidth:'85px', height: '38px', fontSize: '0.9rem' }}
          value={selectedModel}
          onChange={e => setSelectedModel(e.target.value)}
        >
          <option value="all">All</option>
          <option value="artist">Artist</option>
          <option value="track">Track</option>
          <option value="album">Album</option>
        </select>
        <input
          className="form-control"
          type="search"
          placeholder="Search"
          aria-label="Search"
          value={searchQuery}
          onChange={handleSearchChange}
          style={{ height: '38px' }}
        />
      </div>
          {searchResults.length > 0 && (
            <div className="search-results" ref={searchResultsRef} >
              <ul>
                {searchResults.map((result) => (
                  <li key={result.id} onClick={() => handleSearchResultClick(result.id)}>
                    <Link to={`/${result.type}s/${result.id}`}>
                    {`${result.type.charAt(0).toUpperCase() + result.type.slice(1)}: ${result.name}`}
                    </Link>
                  </li>
                ))}
              </ul>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
}

export default Navbar;


