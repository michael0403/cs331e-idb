import React, { useState, useEffect } from 'react'
import './about_us.css'

const AboutUs = () => {
  const [data, setData] = useState({
    memberData: {},
    totals: {},
    yamlData: {}
  })

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_HOST}/about-us/`)
      .then(response => response.json())
      .then(fetchedData =>
        setData({
          memberData: fetchedData.member_data,
          totals: fetchedData.totals,
          yamlData: fetchedData.yaml_data
        })
      )
      .catch(error => console.error('Error fetching about us data:', error))
  }, [])

  const imageUrl = filename =>
    `${process.env.REACT_APP_API_HOST}/static/images/${filename}`

  // Match memberName and memberData of with first two letter of name to make displaying all info easier
  const findMemberStats = memberName => {
    const normalizedMemberName = memberName.substring(0, 2).toLowerCase()
    return Object.entries(data.memberData).find(([key]) =>
      key.toLowerCase().startsWith(normalizedMemberName)
    )?.[1]
  }

  const gitLabUrls = {
    jazmin: 'https://gitlab.com/lystalin',
    michael: 'https://gitlab.com/michael0403',
    rory: 'https://gitlab.com/rlj2379',
    saurelle: 'https://gitlab.com/Saurelle_SWE',
    steven: 'https://gitlab.com/stevencortega'
  }

  return (
    <div>
      <div className='about-us-header mb-4'>About Us</div>

      <p className='text-center'>
        {data.yamlData.project?.info_at_top}
        <br />
        <em>Note:</em> This page tracks
        <span style={{ color: 'red' }}>
          {' '}
          issues created and commits shown on main.
        </span>
        <br />
        I.e., it won't track feature, dev, or hotfix commits until they are
        finalized.
      </p>

      <h3 className='text-center mb-4'>Statistics</h3>
      <div className='container'>
        <div className='row justify-content-center'>
          <div className='col-lg-4 col-md-6 col-sm-12 mb-4'>
            <div className='card'>
              <img
                src={imageUrl('gitlab-logo.png')}
                className='card-img-top img-fluid about-us-img'
                alt='GitLab Logo'
              />
              <div className='card-body'>
                <h5 className='card-title text-center'>Totals</h5>
                <p>
                  Total commits: {data.totals.total_commits}
                  <br />
                  Total issues: {data.totals.total_issues}
                  <br />
                  Total unit tests: {data.totals.total_tests}
                </p>

                <div className='text-center container'>
                  <div className='p-1 row'>
                    <div className='p-1 col'>
                      <a
                        href='https://gitlab.com/michael0403/cs331e-idb/-/wikis/home'
                        className='btn btn-primary text-center'
                        target='_blank'
                        title='Project Wiki'
                      >
                        Project Wiki
                      </a>
                    </div>
                    <div className='p-1 col'>
                      <a
                        href='https://gitlab.com/michael0403/cs331e-idb'
                        className='btn btn-primary text-center'
                        target='_blank'
                        title='GitLab Repository'
                      >
                        Repository
                      </a>
                    </div>
                  </div>
                  <div className='p-1 row'>
                    <div className='p-1 col'>
                      <a
                        href='https://gitlab.com/michael0403/cs331e-idb/-/issues'
                        className='btn btn-primary text-center'
                        target='_blank'
                        title='GitLab Issues'
                      >
                        Issue Tracker
                      </a>
                    </div>
                    <div className='p-1 col'>
                      <a
                        href='https://docs.google.com/presentation/d/1uDD58rDYjlnay8zzfcxda0ZU5FxZtRDIFJXxcYyQJVM/edit?usp=sharing'
                        className='btn btn-primary text-center'
                        target='_blank'
                        title='Project Presentation'
                      >
                        Project Presentation
                      </a>
                    </div>
                  </div>
                  <div className='p-1 row'>
                    <div className='p-1 col'>
                      <a
                        href='https://documenter.getpostman.com/view/33692556/2sA3BhctX8'
                        className='btn btn-primary text-center'
                        target='_blank'
                        title='Postman Docs'
                      >
                        Postman Docs
                      </a>
                    </div>
                    <div className='p-1 col'>
                      <a
                        href= 'https://music-mosaic.postman.co/workspace/MusicMosaic~1fb34bd0-0928-4225-81ac-b05b05acc69c/collection/33692556-da607a7a-d32b-4365-9c69-e87d80f6eb98?action=share&creator=33692556'
                        className='btn btn-primary text-center'
                        target='_blank'
                        title='Postman Tests'
                      >
                        Postman Tests
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          {Object.entries(data.yamlData)
            .filter(([key]) => key !== 'project')
            .map(([key, member]) => {
              const memberStats = findMemberStats(key)

              return (
                <div className='col-lg-4 col-md-6 col-sm-12 mb-4' key={key}>
                  <div className='card'>
                    <img
                      src={imageUrl(member.pic_filename)}
                      className='card-img-top img-fluid about-us-img'
                      alt={`Picture of ${key}`}
                    />
                    <div className='card-body'>
                      <h5 className='card-title text-center'>
                        {key.charAt(0).toUpperCase() + key.slice(1)}
                      </h5>
                      <p>{member.bio}</p>

                      <em>Responsibilities:</em>
                      <ul>
                        {member.responsibilities.map((responsibility, idx) => (
                          <li key={idx}>{responsibility}</li>
                        ))}
                      </ul>

                      <em>Statistics:</em>
                      <br />
                      <p>
                        Commits: {memberStats?.num_commits ?? 'N/A'}
                        <br />
                        Issues: {memberStats?.num_issues ?? 'N/A'}
                        <br />
                        Unit tests: {member.unit_tests ?? 'N/A'}
                      </p>
                      {gitLabUrls[key] && (
                        <div className='text-center mt-3'>
                          <a
                            href={gitLabUrls[key]}
                            className='btn btn-primary'
                            target='_blank'
                            rel='noopener noreferrer'
                            title={`${
                              key.charAt(0).toUpperCase() + key.slice(1)
                            }'s GitLab`}
                          >
                            GitLab
                          </a>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              )
            })}
        </div>
      </div>
      {data.yamlData.project && (
        <div className='container mt-5'>
          <div className='row'>
            <div className='col-12'>
              <div className='pt-5' id='about-us-datatools'>
                <h3 className='text-center about-us-header'>Data and Tools</h3>
                <div className='p-3'>
                  <h3>Data</h3>
                  <p>{data.yamlData.project.data_info}</p>
                  <h4>Data Sources</h4>
                  <p>
                    {Object.entries(data.yamlData.project.data_list).map(
                      ([api, link]) => (
                        <React.Fragment key={api}>
                          <a
                            href={link}
                            target='_blank'
                            rel='noopener noreferrer'
                          >
                            {api}
                          </a>
                          <br />
                        </React.Fragment>
                      )
                    )}
                  </p>
                </div>
                <div className='p-2'>
                  <h3>Tools</h3>
                  <p>{data.yamlData.project.tools_info}</p>
                  <h4>List of Tools Used</h4>
                  <p>
                    {data.yamlData.project.tools_list.map(tool => (
                      <React.Fragment key={tool}>
                        {tool}
                        <br />
                      </React.Fragment>
                    ))}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  )
}

export default AboutUs
