import React from 'react'; 
import 'bootstrap/dist/css/bootstrap.css'; 
import Pagination from 'react-bootstrap/Pagination'; 

function ModelPagination({ pageNum, totalPages, setPageNum}) { 

return ( 
	<ul className="pagination justify-content-center">
		<Pagination> 
			<Pagination.Prev 
				className = {pageNum === 1 ? 'disable' : ''}
				onClick = {
					() => {
	          setPageNum(pageNum - 1)
	        }}
				disabled = {pageNum === 1}
			/> 
			 <Pagination.Item>
       &nbsp;Current Page: {pageNum}&nbsp;
       </Pagination.Item>
			<Pagination.Next 
			  className = {pageNum === totalPages ? 'disable' : ''}
				onClick = {
					() => {
	          setPageNum(pageNum + 1)
	        }}
				disabled = {pageNum === totalPages}
			/> 
		</Pagination> 
	</ul>
); 
}


export default ModelPagination