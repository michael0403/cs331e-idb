import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import HomePage from './components/home/home';
import Aboutus from './components/about/about_us';
import ArtistSearch from './components/artist/artist_search';
import ArtistModel from './components/artist/artist_model';
import AlbumModel from './components/album/album_model';
import AlbumSearch from './components/album/album_search';
import TrackSearch from './components/tracks/tracks_search';
import TrackModel from './components/tracks/tracks_model';
import Navbar from './components/navbar';
import './App.css';

function App() {
  return (
    <Router>
      <Navbar />
      <Routes>
        <Route path="/" element={<HomePage />} exact />
        <Route path="/about-us" element={<Aboutus />} />
        <Route path="/artists" element={<ArtistSearch />} />
        <Route path="/artists/:id" element={<ArtistModel />} />
        <Route path="/albums" element={<AlbumSearch />} />
        <Route path="/albums/:id" element={<AlbumModel />} />
        <Route path="/tracks" element={<TrackSearch />} />
        <Route path="/tracks/:id" element={<TrackModel />} />
      </Routes>
    </Router>
  );
}

export default App;
